# Generated by Django 4.1.7 on 2023-03-30 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0010_alter_bookings_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookings',
            name='is_active',
            field=models.IntegerField(choices=[(1, '预约'), (2, '已签到'), (3, '未签到'), (4, '已取消')], default=1, verbose_name='活跃状态'),
        ),
    ]
